#! META
name: fwmultik_stat_monitoring
description: List how many cores are used and connection per core
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
    role-firewall: true
    this_tag_disables_this_script: this_is_intentional

#! COMMENTS
corexl-cpu-connections:
    why: |
        The number of connections per core could vary. However if they are very undistributed it could cause issues with one core being overloaded while others are not.
    how: |
        By using the Check Point built-in "fw ctl multik stat" command, the amount of connections per CPU core is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing amount of connections per CPU core is only available from the command line interface.

corexl-cores-enabled:
    why: |
        Members of the same cluster must have the same amount of CPU cores enabled, otherwise they will not function correctly together.
    how: |
        By using the Check Point built-in "fw ctl multik stat" command, the number of active CPU cores is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the amount of active CPU cores is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 fw ctl multik stat

#! PARSER::AWK

BEGIN {
	FS="|"
	icpu=0
}

#  0 | Yes     | 11     |        2722 |    15702
/Yes/ {
	connections=$4
	coreXLTags["cpu-id"] = $1
	writeDoubleMetric("corexl-cpu-connections", coreXLTags, "gauge", 300, trim(connections))
	icpu++
	coreXL=1
}

END {
	if ( coreXL == 1 ) {
		writeComplexMetricStringWithLiveConfig("corexl-cores-enabled", null, icpu, "CoreXL - Cores Enabled")
	}
}
