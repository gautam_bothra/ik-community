#! META
name: chkp-os-verify_etc-hosts
description: Verifies that the hostname is present in the /etc/hosts file
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
        -
            os.name: gaia-embedded
        -
            os.name: ipso
            
#! COMMENTS
hostname-exists-etc-hosts:
    why: |
        It is very important that there is an entry in the /etc/hosts file with the hostname of the device and one of its IP addresses. The IP address used is the one for the interface set as "management interface". If the IP address is removed for this interface, the /etc/hosts file will be lacking this entry, which will cause Check Point services to malfunction. If the device is rebooted while this happens, Check Point services will not start at all and the device will need to be accessed from serial console or LOM.
    how: |
        By making sure that the hostname of the device is present in the /etc/hosts file the issue can be discovered right away, and corrected, before any negative impact.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 echo "hostname: `hostname`" && cat /etc/hosts

#! PARSER::AWK

BEGIN {
	hostname_exists = 0
}

# hostname: lab-CPMGMTR7730
/^hostname:/ {
	hostname = $2
}

# 127.0.0.1 localhost
# 10.10.6.10 lab-CPMGMTR7730
# 10.10.6.27      lab-CP-GW6-R77SPLAT     lab-CP-GW6-R77SPLAT.test.local
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {

	if ( $0 ~ hostname ) {
		hostname_exists = 1
	}
}

END {
	writeDoubleMetric("hostname-exists-etc-hosts", null, "gauge", 300, hostname_exists)
}
