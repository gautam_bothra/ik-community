#! META
name: chkp-os-dst-cache
description: checks the current cache and the limit
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
        # os.name: gaia-embedded removed per   IKP-932
        
#! COMMENTS
destination-cache-usage:
    why: |
        The destination cache is used to remember routing decisions that were made by the firewall to accelerate future routing decisions. If the cache reaches its limit performance issues may occur.
    how: |
        The current cache usage is taken from /proc/slabinfo and the limit from /proc/sys/net/ipv4/route/max_size.
        See Check point KB https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk100110 for more information.
    without-indeni: |
        An administrator could login and manually check this from the command line interface.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

destination-cache-limit:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 cat /proc/slabinfo |grep ip_dst_cache && echo -n "max " && ${nice-path} -n 15 cat /proc/sys/net/ipv4/route/max_size

#! PARSER::AWK

# ip_dst_cache          20     48    320   12    1 : tunables   54   27    8 : slabdata      4      4      0
/ip_dst_cache/ {
	writeDoubleMetric("destination-cache-usage", null, "gauge", 300, $3)
}


# max 1048576
/max / {
	writeDoubleMetric("destination-cache-limit", null, "gauge", 300, $2)
}
