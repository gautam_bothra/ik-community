#! META
name: chkp-os-installed_jumbo_take
description: run "installed_jumbo_take"
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    os.name: gaia

#! COMMENTS
hotfix-jumbo-take:
    why: |
        It is very important to make sure that devices are patched with the latest versions and hotfixes, to prevent downtime and security incidents.
    how: |
        Using the Check Point command "installed_jumbo_take" we retreive the currently installed jumbo hotfixes.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing installed hotfixes is only available from the command line interface, and in some cases also via the WebUI and SmartUpdate.

#! REMOTE::SSH
${nice-path} -n 15 installed_jumbo_take -n; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "CPUpdates/6.0/BUNDLE_FIAT_HF_BASE_026" SU_Build_Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "Check Point Mini Suite/setup/FIAT_HF_BASE_026" Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "CPUpdates/6.0/BUNDLE_GIZMO_HF_041_050" SU_Build_Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "Check Point Mini Suite/setup/GIZMO_HF_041_050" Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "CPUpdates/6.0/BUNDLE_GULLI_HF_BASE_008" SU_Build_Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "Check Point Mini Suite/setup/GULLI_HF_BASE_008" Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "CPUpdates/6.0/BUNDLE_GYPSY_HF_BASE_021" SU_Build_Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "Check Point Mini Suite/setup/GYPSY_HF_BASE_021" Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "CPUpdates/6.0/BUNDLE_R77_20_JUMBO_HF" SU_Build_Take 0; ${nice-path} -n 15 $CPDIR/bin/cpprod_util CPPROD_GetValue "Check Point Mini Suite/setup/R77_20_jumbo_hf" Take 0 

#! PARSER::AWK

############
# Script explanation: The method for determining the jumbo take changes between major versions, and also between jumbo hotfix versions. 
# Check point KB: sk98028
# Currently supports: R75.47, R76, R77, R77.10, R77.20, R77.30, R80 (and probably future versions as well since the command is the same after R77.30)
###########

# 159
/^[0-9]+/ {
	writeComplexMetricString("hotfix-jumbo-take", null, $1)
}
