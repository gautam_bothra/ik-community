#! META
name: junos-show-system-license
description: JUNOS get licenses
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
license-elements-limit:
    why: |
       Tracking the utilization of licenses is important to ensure smooth operation of critical functions. If a license is limited by count (such as by number of users) and the limit is reached, service disruption may follow.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show system license" command. The output includes the device's installed licenses.
    without-indeni: |
       The Juniper device can be set to send an SNMP trap when a license nears expiration (or expires), or is nearing its capacity.
    can-with-snmp: true
    can-with-syslog: false
license-elements-used:
    skip-documentation: true
features-enabled:
    why: |
        SRX devices have many features which can be enable based on the licenses installed on the devices.
    how: |
        The script runs "show system license" command via SSH connection to the device to list the features enabled on the device.
    without-indeni: |
        An administrator could log on to the device to manually run this command to display which features are enabled on the device.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The enabled features can be retrieved from the command line and GUI.

license-expiration:
    why: |
        Most of the features on SRX devices need licenses.
    how: |
        The script runs "show system license" command via SSH connnection to the device to retrieve the license expiration information.

    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The license expiration information can be retrieved from the command line and GUI.

#! REMOTE::SSH
show system license | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//license-summary-information[1]
_metrics:
    -
        _groups:
            ${root}/license-usage-summary/feature-summary:
                _tags:
                    "im.name":
                        _constant: "license-elements-limit"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Licenses - Limit"
                    "im.dstype.displayType":
                        _constant: "number"
                    "name":
                        _text: name
                _value.double:
                    _text: licensed
    -
        _groups:
            ${root}/license-usage-summary/feature-summary:
                _tags:
                    "im.name":
                        _constant: "license-elements-used"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Licenses - Used"
                    "im.dstype.displayType":
                        _constant: "number"
                    "name":
                        _text: name
                _value.double:
                    _text: used-licensed
    -
        _groups:
            ${root}/license-information/license:
                _tags:
                    "im.name":
                        _constant: "features-enabled"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Features Enabled"
                _value.complex:
                    name:
                        _text: feature-block/feature/name
        _value: complex-array
    -
        _groups:
            ${root}/license-information/license:
                _tags:
                    "im.name":
                        _constant: "license-expiration"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "License Expire"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _attribute:
                        _name: "seconds"
                        _path: "feature-block/feature/validity-information/end-date"
                _temp:
                    name:
                        _text: feature-block/feature/name
        _transform:
            _tags:
                "name": |
                    {
                        feature = temp("name")
                        print feature
                    }

