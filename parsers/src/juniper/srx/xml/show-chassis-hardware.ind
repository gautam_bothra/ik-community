#! META
name: junos-show-chassis-hardware
description: JUNOS show chassis hardware
type: monitoring
monitoring_interval: 1440 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
serial-numbers:
    why: |
       Capture the device's serial number as well as important components' serial numbers. This makes inventory tracking easier.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show chassis hardware" command. The output includes the device's hardware and serial number details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show chassis hardware | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//chassis-inventory[1]
_metrics:
    -
        _groups:
            ${root}/chassis/chassis-module[serial-number]:
                _tags:
                    "im.name":
                        _constant: "serial-numbers"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Serial Numbers"
                _value.complex:
                    name:
                        _text: "name"
                    "serial-number":
                        _text: "serial-number"
        _value: complex-array
