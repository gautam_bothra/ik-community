#! META
name: junos-show-route-protocol-static-terse
description: JUNOS get static routes information 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
connected-networks-table:
    why: |
        The static routes are manually defined on a device. Incorrectly defined static routes will cause the network outage or unpredictable network behaviors.
    how: |
        This script retrieves routes statically defined on a device by running the command "show route protocol static terse" via SSH connection to a device. 
    without-indeni: |
        An administrator could log on to the device to run the command "show route protocol static terse" to collect the same information.
    can-with-snmp: false 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve this information

#! REMOTE::SSH
show route protocol static terse

#! PARSER::AWK
#inet.0: 13 destinations, 13 routes (13 active, 0 holddown, 0 hidden)
/inet/ {
  table_name = $1
}

#* 30.30.30.0/24      D   0                       >lt-0/0/0.1
/^(\*\s+[0-9]|\+\s+[0-9]|\-\s+[0-9])/ {
    line++
    network = $2
    next_hop = $NF
    split(network, network_prefix, "/")
    route[line, "table-name"] = table_name
    route[line, "network"] = network_prefix[1]
    route[line, "mask"] = network_prefix[2]  
    gsub(/\>/, "", next_hop)
    route[line, "next-hop"] = next_hop
}

END{
    writeComplexMetricObjectArray("static-routing-table", null, route)
}
