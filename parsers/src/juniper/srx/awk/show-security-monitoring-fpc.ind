#! META
name: junos-show-security-monitoring-fpc
description: JUNOS SRX connection per second for last 96 seconds 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
connections-per-second:
    why: |
        connections per second
    how: |
        This script retrieves the average connections per second for the last 96 seconds on a particular fpc via SSH connection to the SRX device by running "show security monitoring fpc X" command. X represents which fpc is. 
    without-indeni: |
        An administrator could log on to the device to run the command "show security monitoring fpc 0" to collect the same information.
    can-with-snmp: false 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve this information

#! REMOTE::SSH
show security monitoring fpc 0 

#! PARSER::AWK
BEGIN{
   label_tag["cluster"] = "false"
}
 
#node0:
/^(node)/{
   label_tag["cluster"] = "true" 
   lable_tag["node"] = $1
}

#IPv4  Session Creation Per Second (for last 96 seconds on average):  128
/^(IPv4\s+Session\s+Creation\s+Per\s+Second)/{
   CPS = $NF 
   writeDoubleMetric("connections-per-second", lable_tag, "gauge", 60, CPS)
}
