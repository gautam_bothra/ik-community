#! META
name: ios-show-ip-route-connected
description: IOS show version
type: monitoring 
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios 

#! COMMENTS
connected-networks-table:
    why: |
       Capture the route entries that are directly connected to the device.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show ip route connected" command. The output includes a table with the device's directly attached networks.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show ip route connected

#! PARSER::AWK

BEGIN {
    preferred = "0.0"
    entry = 0
}

# SAMPLE output
#Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
#       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area 
#       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
#       E1 - OSPF external type 1, E2 - OSPF external type 2
#       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
#       ia - IS-IS inter area, * - candidate default, U - per-user static route
#       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
#       a - application route
#       + - replicated route, % - next hop override
#
#Gateway of last resort is 173.36.208.125 to network 0.0.0.0
#S*    0.0.0.0/0 [1/0] via 173.36.208.125
#      10.0.0.0/8 is variably subnetted, 43 subnets, 2 masks
#C        10.1.101.0/24 is directly connected, GigabitEthernet0/0/1.101
#L        10.1.101.1/32 is directly connected, GigabitEthernet0/0/1.101
#C        10.1.111.0/24 is directly connected, GigabitEthernet0/0/1.111

/[\*]|^[C]|^[L]/ {

    line = trim($0)
	leftchars = substr($0, 1, 2)
    split(line, field, " {1,}")

    if ( leftchars ~ /[\*]/ ) { preferred = "1.0" }
    if ( (leftchars ~ /^C/ || leftchars ~ /^L/) && $0 !~ /Codes:/  )	{ 
        writeDebug($0)
        split(trim(field[2]),val,"/")  
        network=trim(val[1])
        mask=trim(val[2]) 
    }
    
    # add metrics to table ...
    if ( network != "" && mask != "") { addMetrics() }
    
}

function addMetrics () {

    if ( mask != "32" ) {
        entry++
        routes[entry, "network"] = network
        routes[entry, "mask"] = mask
    }
}

function writeMetrics () {
    writeComplexMetricObjectArrayWithLiveConfig("•connected-networks-table ", null, routes, "Directly Connected Networks")
}

END {
    writeMetrics()
}
