#! META
name: nexus-show-hsrp
description: fetch the HSRP status
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    hsrp_engine: true

#! COMMENTS
cluster-member-active:
    why: |
       Check if the device is the currently active HSRP (Hot Standby Router Protocol) member. The active router is acting as the gateway for the HSRP group.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configure interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

cluster-state:
    why: |
       Check if a configured HSRP (Hot Standby Router Protocol) group has at least one active member. If no active members exist traffic would not be able to be routed. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configure interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Only state transitions generate a syslog event. There is no explicit event for the last member failing.
    can-with-snmp: true
    can-with-syslog: false

cluster-preemption-enabled:
    why: |
       Check if an HSRP (Hot Standby Router Protocol) group has preemption enabled. If preemption is enabled then a recovering device can trigger a switchover which may create a short interruption in traffic forwarding. It is recommended to disable preemption.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configure interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Preemption events would generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

virtual-redundancy-groups:
    why: |
       Check if the HSRP (Hot Standby Router Protocol) groups are synchronized across a vPC cluster. It is expected that all HSRP groups would be the same across 2 vPC peers. The HSRP groups should use the same virtual IP, VLAN and group number.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configure interfaces.
    without-indeni: |
       Correlating all the HSRP groups across both vPC members can only be done manually, comparing the HSRP configuration.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show hsrp | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            ${root}/TABLE_grp_detail/ROW_grp_detail:
                _temp:
                    state:
                        _text: "sh_group_state"
                    grp_name:
                        _text: "sh_ip_redund_name"
                    grp_ip:
                        _text: "sh_vip"
                _tags:
                    "im.name":
                        _constant: "cluster-member-active" 
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "HSRP - This Member State"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"             
        _transform:
            _value.double:  |
                {
                    if (temp("state") == "Active") { print "1.0" } else { print "0.0" }
                }
            _tags:
                "name": |
                    { print temp("grp_name") ":" temp("grp_ip") }
    -
        _groups:
            ${root}/TABLE_grp_detail/ROW_grp_detail:
                _temp:
                    active_rtr:
                        _text: "sh_active_router_addr"
                    grp_name:
                        _text: "sh_ip_redund_name"
                    grp_ip:
                        _text: "sh_vip"
                _tags:
                    "im.name":
                        _constant: "cluster-state"
                    "name":
                        _text: "sh_ip_redund_name"
                    "group-vip":
                        _text: "sh_vip" # HSRP Virtual IP
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "HSRP - Cluster State"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"             
        _transform:
            _value.double:  |
                {
                    if (temp("active_rtr") != "") { print "1.0" } else { print "0.0" }
                }
            _tags:
                "name": |
                    { print temp("grp_name") ":" temp("grp_ip") }
    -
        _groups:
            ${root}/TABLE_grp_detail/ROW_grp_detail:
                _temp:
                    preempt:
                        _text: "sh_preempt"
                    grp_name:
                        _text: "sh_ip_redund_name"
                    grp_ip:
                        _text: "sh_vip"
                _tags:
                    "im.name":
                        _constant: "cluster-preemption-enabled" 
                    "name":
                        _text: "sh_ip_redund_name"
                    "group-vip":
                        _text: "sh_vip" # HSRP Virtual IP
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "HSRP - Preemption"
                    "im.dstype.displayType":
                        _constant: "boolean"            
                    "im.identity-tags":
                        _constant: "name"             
        _transform:
            _value.double:  |
                {
                    if (temp("preempt") == "enabled") { print "1.0" } else { print "0.0" }
                }
            _tags:
                "name": |
                    { print temp("grp_name") ":" temp("grp_ip") }
    -
        _groups:
            ${root}/TABLE_grp_detail/ROW_grp_detail:
                _temp:
                    grp_name:
                        _text: "sh_ip_redund_name"
                    grp_ip:
                        _text: "sh_vip"
                _tags:
                    "im.name":
                        _constant: "virtual-redundancy-groups" 
                    "name":
                        _constant: "hsrp"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "HSRP - Cluster Groups"
                    "im.identity-tags":
                        _constant: "name"             
        _transform:
            _value.complex:
                virtual-group: |
                    { print temp("grp_name") ":" temp("grp_ip") }
        _value: complex-array 
