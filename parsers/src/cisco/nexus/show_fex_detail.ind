#! META
name: nexus-status-fex
description: Nexus show fex state
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    fex: true

# -------------------------------------------------------------------------------------------------
# The script publish the "fex-status-alarm" for each CISCO Nexus 2k Series switches (FEX) based on the output of the 'show fex detail' command
# The value depends from the fex status. If the status is not "online" then the value is true.
#
# Apart from the alarm value, the following tags are published.
#   1. FEX name (is 'FEX-XXX' where XXX is the fex number)
#   2. FEX status (Online | Offline | Discovered | AA Version Mismatch |... )
#   3. FEX model
#   4. FEX serial
# -------------------------------------------------------------------------------------------------


#! COMMENTS
fex-status-alarm:
    why: |
        Monitor the state of the FEX which is useful for detecting misconfigurations and problems to the Fabric Extenders. If a problem is detected, Indeni Server issues an alert to mitigate the fault and minimize potential network outages.
    how: |
        This script logs into the Cisco Nexus parent switches through SSH and retrieves the FEX status along FEX name, model, and S/N by using the "show fex detail" command.
    without-indeni: |
        It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: True
    can-with-syslog: True

#! REMOTE::SSH
show fex detail

#! PARSER::AWK

# ------------------------------------------------------------------
# A utility function that returns the part of the string "stringToReadFrom"
# that is between the parameter strings "startStart" and "stringEnd"
# ------------------------------------------------------------------
function getStringBetween(stringToReadFrom, stringStart, stringEnd) {

    indexStart = match(stringToReadFrom, stringStart)
    indexStart = indexStart + length(stringStart)

    if (stringEnd == "") {
        indexEnd = length(stringToReadFrom) + 1
    } else {
        indexEnd = match(stringToReadFrom, stringEnd)
    }

     if (indexStart==0 || indexEnd ==0 || indexEnd < indexStart) {

            # The "stringStart" OR "stringEnd" is not part of the "stringToReadFrom".
            # Or invalid positions "stringStart" is after "stringBefore"
            return ""

     } else {

        lengthOfString = (indexEnd - indexStart)
        return trim(substr(stringToReadFrom, indexStart, lengthOfString))

    }
}


BEGIN {
    # An array contains all the needed fex data.
    # Initialize the index of the array
    array_index = 0
}

#FEX: 100 Description: FEX0100   state: Online
#FEX: 112 Description: FEX2k - under N5kUP   state: AA Version Mismatch
/^FEX:/ {
    # The beginning of a FEX structure. We will parse number, description, state
    array_index++
    array_fex_info[array_index, "number"] = getStringBetween($0, "FEX:", " Description: ")
    array_fex_info[array_index, "description"] = getStringBetween($0, " Description: ", " state: ")
    array_fex_info[array_index, "state"] = getStringBetween($0, " state: ", "")
}

#  Extender Serial: FOX1842GGY3
/Extender Serial:/ {
    array_fex_info[array_index, "serial"] = $NF
}

#  Extender Model: N2K-C2248TP-1GE,  Part No: 73-13232-02
/Extender Model:/ {
    array_fex_info[array_index, "model"] = getStringBetween($0, " Model:", ",")
}

END {

    # For each FEX publish the alarm
    array_size = array_index + 1

    for (i = 1; i < array_size; i++) {

        fex_tags_to_publish["name"] = "FEX-" array_fex_info[i, "number"]
        fex_tags_to_publish["state"] = array_fex_info[i, "state"]
        fex_tags_to_publish["serial"] = array_fex_info[i, "serial"]
        fex_tags_to_publish["model"] = array_fex_info[i, "model"]

        # The value of the alarm depends from the value of the fex-state
        fex_status_alarm_value = 1
        if (array_fex_info[i, "state"] == "Online") {
            fex_status_alarm_value = 0
        }
        writeDoubleMetric("fex-status-alarm", fex_tags_to_publish, "gauge", 60, fex_status_alarm_value)
    }
}

