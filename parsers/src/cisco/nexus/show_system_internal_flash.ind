#! META
name: nexus-show-system-internal_flash
description: fetch disk status
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
disk-used-kbytes:
    skip-documentation: true

disk-total-kbytes:
    skip-documentation: true

disk-usage-percentage:
    why: |
       Check the utilization percentage of each of the diffenet file systems on the device. If the free space gets too low an alert will be triggered.
    how: |
       This script logs into the Cisco Nexus switch using SSH and uses the "show system internal flash" command to collect information about the device's file systems.
    without-indeni: |
       It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show system internal flash | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _groups:
            ${root}/TABLE_flash/ROW_flash:
                _temp:
                    used_bytes:
                        _text: "used"
                _tags:
                    "im.name":
                        _constant: "disk-used-kbytes"
                    "file-system":
                        _text: "Mounted-on"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "File System - Used"
                    "im.identity-tags":
                        _constant: "file-system"   
                    "im.dstype.displayType":
                        _constant: "kilobytes"
        _transform:
            _value.double: |
                { print temp("used_bytes") / 1024 }
    -
        _groups:
            ${root}/TABLE_flash/ROW_flash:
                _temp:
                    used_bytes:
                        _text: "used"
                    free_bytes:
                        _text: "Available"
                _tags:
                    "im.name":
                        _constant: "disk-total-kbytes"
                    "file-system":
                        _text: "Mounted-on"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "File System - Total"
                    "im.identity-tags":
                        _constant: "file-system"  
                    "im.dstype.displayType":
                        _constant: "kilobytes"           
        _transform:
            _value.double: |
                { print (temp("used_bytes") + temp("free_bytes")) / 1024 }

    -
        _groups:
            ${root}/TABLE_flash/ROW_flash:
                _value.double:
                    _text: "Use-percent"
                _tags:
                    "im.name":
                        _constant: "disk-usage-percentage"
                    "file-system":
                        _text: "Mounted-on"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "File System - Usage"
                    "im.identity-tags":
                        _constant: "file-system"   
                    "im.dstype.displayType":
                        _constant: "percentage"          
