#! META
name: nexus-show-ip-bgp-summary
description: Nexus show ip route direct
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    bgp: true

#! COMMENTS
bgp-state:
    why: |
       Check if BGP neighbors are up. If a neighbor is not in established state an alert will get triggered.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the BGP neigborship state using the "show ip bgp summary" command. The output includes a complete report of the per-neighbor BGP state.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

bgp-received-routes:
    why: |
       Check if a BGP neighbor's received routes changed in a substantial way. If the received routes number changed by +/- 20% an alert will be raised.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the BGP neighborship state using the "show ip bgp summary" command. The output includes a complete report of the per-neighbor BGP state.
    without-indeni: |
       It is possible to poll this data through SNMP but SNMP would not report changes.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
show ip bgp summary

#! PARSER::AWK

BEGIN {
    i = 0
}

#Neighbor        V    AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
/Neighbor.*State/ {
    getColumns(trim($0), "[ \t]+", columns)
}

#192.168.100.2   4   100    6183    6183       19    0    0    4d06h 3
#192.168.101.3   4   100      22      29        0    0    0    4d06h Idle
/^[0-9]+\.[0-9]+\./ {
    peerCol = getColId(columns, "Neighbor")
    stateCol = getColId(columns, "State/PfxRcd")

    peer = trim($peerCol)
    state = trim($stateCol)

    bgptags["name"] = peer

    if (state ~ /[0-9]+/) {
        bool_state = "1.0"
        rcvd_routes = state
    } else {
        bool_state = "0.0"
        rcvd_route = "0.0"
    }

    writeDoubleMetric("bgp-state", bgptags, "gauge", 60, bool_state)
    writeDoubleMetric("bgp-received-routes", bgptags, "gauge", 60, rcvd_routes)
}
