#! META
name: panos-show_system_logdb-quota
description: fetch log utilization
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
logdb-usage:
    why: |
        The log DB stores a variety of different log types on a Palo Alto Networks device. Most log databases will auto-purge older logs. In many environments, though, such behavior is not desired. Users should know if they are reaching the maximum amount of logs they can retain of a certain type and assess the possible impact.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current status of the log DBs  (the equivalent of running "show system logdb-quota" in CLI).
    without-indeni: |
        An administrator could write a script to leverage the Palo Alto Networks API to collect this data periodically and alert appropriately. The web interface can also be used to check the current status of the log DB utilization.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><system><logdb-quota></logdb-quota></system></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK
BEGIN {
}

#               system: 4.00%, 0.664 GB
/(\d+)%/ {
	logtype=$1
	sub(/\:/, "", logtype)
    usage = $2
    gsub(/[%,]/, "", usage)

    logtags["name"] = logtype

    writeDoubleMetricWithLiveConfig("logdb-usage", logtags, "gauge", "600", usage, "Log Usage", "percentage", "name")
}

