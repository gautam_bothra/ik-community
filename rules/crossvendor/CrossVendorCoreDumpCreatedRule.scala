package com.indeni.server.rules.library.crossvendor

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, Equals, Not, Or}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.language.reflectiveCalls

case class CrossVendorCoreDumpCreatedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_core_dump_created", "All Devices: Core dump files found",
    "A core dump is created when a process crashes. Indeni will alert when a core dump file is created.", AlertSeverity.ERROR)
    .build()

  override def expressionTree: StatusTreeExpression = {
    val currentValue = SnapshotExpression("core-dumps").asMulti().mostRecent().noneable
    val previousValue = SnapshotExpression("core-dumps").asMulti().middle()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("core-dumps")).multi(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        Or(
          And(
            previousValue.isEmpty,
            Not(Equals(currentValue, previousValue))
          ),
          And(
            currentValue.nonEmpty,
            Not(Equals(currentValue, previousValue))
          )
        )
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("A core dump file has been created. This happens when a process crashes, and the core dump can contain information on why this happened. This usually means that there is an issue that should be investigated."),
      ConditionalRemediationSteps("The list of core dumps is retrieved by logging into the shell over SSH and retrieving the details of files found in /var/log/dump/usermode/, /var/tmp/*core* or /var/crash. Investigate the core dump files. If the issue is not clear, open up a case with vendor support and send them the file.",
        ConditionalRemediationSteps.VENDOR_F5 ->
          """Login to your device with SSH and run "ls /var/core". Investigate the core dump files.
            |If the cause of the issue is not clear, open up a case with F5 and follow the instructions on this page in order to provide them with the information needed:
            |<a target="_blank" href="https://support.f5.com/csp/article/K10062">Article: K10062 on AskF5</a>.""".stripMargin
      )
    )
  }
}
