package com.indeni.server.rules.library.templatebased.loadbalancer

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class lb_server_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "lb_server_down",
  ruleFriendlyName = "Load Balancers: Server(s) down",
  ruleDescription = "indeni will alert one or more servers that the load balancer is directing traffic to is down.",
  metricName = "lb-server-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Servers Affected",
  alertDescription = "One or more servers, to which this device is forwarding traffic, are down.",
  baseRemediationText = "Review the cause for the servers being down.")()
