package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class checkpoint_compare_ccp_mode(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_ccp_mode",
  ruleFriendlyName = "Check Point Cluster: ClusterXL CCP mode mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the CCP mode settings are different.",
  metricName = "clusterxl-ccp-mode",
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same CCP mode settings.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = """Compare the output of "cphaprob -a if" across members of the cluster.""")()
