package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class f5_ssl_weak_cipher(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "f5_ssl_weak_cipher",
  ruleFriendlyName = "F5 Devices: Weak cipher used with SSL profiles",
  ruleDescription = "Certain ciphers are now considered weak. indeni will alert if any SSL profiles are set to use them.",
  metricName = "ssl-weak-cipher",
  applicableMetricTag = "name",
  descriptionMetricTag = "cipher",
  alertIfDown = false,
  alertItemsHeader = "Affected Profiles",
  alertDescription = "Certain SSL profiles have a weak cipher and are potentially opening traffic to known vulnerabilities.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Follow the knowledge articles listed in the affected profiles above. Since F5 devices present the attributes in alphabetical order (to the other side), be careful when adding a property.",
  itemSpecificDescription = Seq(
    ".*DES-CBC3.*".r -> "The DES-CBC3 cipher is considered weak (vulnerability SWEET32). See https://support.f5.com/csp/#/article/K13167034",
    ".*RC4.*".r -> "The RC4 cipher is considered weak. See https://support.f5.com/csp/#/article/K16864",
    ".*".r -> "")
)()
