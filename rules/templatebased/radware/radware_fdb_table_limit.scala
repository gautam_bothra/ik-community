package com.indeni.server.rules.library.templatebased.radware

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class radware_fdb_table_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "radware_fdb_table_limit",
  ruleFriendlyName = "Radware Alteon: FDB table usage high",
  ruleDescription = "The Alteon Forwarding Data Base table maps a MAC address (for a given VLAN) with the port number for that MAC address. indeni will alert prior to the table reaching its limit.",
  usageMetricName = "fdb-usage",
  limitMetricName = "fdb-limit",
  threshold = 80.0,
  alertDescriptionFormat = "The FDB table usage is %.0f where the limit is %.0f.",
  baseRemediationText = "Review https://kb.radware.com/Questions/Alteon/Public/Alteon-Forwarding-Data-Base-(FDB)-Table")()
