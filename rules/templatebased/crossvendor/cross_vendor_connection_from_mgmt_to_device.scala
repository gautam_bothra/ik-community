package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.EndsWithRepetition
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_connection_from_mgmt_to_device(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_connection_from_mgmt_to_device",
  ruleFriendlyName = "All Devices: Communication between management server and specific devices not working",
  ruleDescription = "A management server needs to communicate with its managed devices at all times. indeni will alert if the communication is broken.",
  metricName = "trust-connection-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Unreachable Managed Devices",
  alertDescription = "Some of the devices managed by this device cannot be reached by the management device. " +
    "Please review the list below. Note that the list may include devices that are not covered by indeni at this " +
    "point, as the check is done from the management server to all managed devices.",
  historyLength = 2,
  baseRemediationText = "Troubleshoot any possible connectivity issues.") (
  ConditionalRemediationSteps.VENDOR_CP -> "Read https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk60522"
)
