package com.indeni.server.rules.library.checkpoint

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class ChkpFwLogFileIncreaseRateHighRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("chkp_fw_log_file_increase_rate_high", "Check Point Firewalls: Firewall logging locally",
    "Indeni will track the size of the fw.log file to detect when the firewall is logging locally.", AlertSeverity.ERROR)
    .interval(TimeSpan.fromMinutes(10))
    .build()

  override def expressionTree: StatusTreeExpression = {
    val lastValue = TimeSeriesExpression[Double]("log-file-size").last
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("log-file-size"), historyLength = ConstantExpression(TimeSpan.fromHours(1))),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        GreaterThan(lastValue, ConstantExpression(Some(0.0)))
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      scopableStringFormatExpression("The $FWDIR/log/fw.log file is growing. This indicates local logging."),
      ConditionalRemediationSteps("Determine why this is occurring - it could potentially be a connectivity issue to the log server, or too many logs being generated per second.",
        ConditionalRemediationSteps.VENDOR_CP -> "Review <a target=\"_blank\" href=\"https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk40090\">Solution ID sk40090 on Check Point Support Center</a>."
      )
    )
  }
}
